import axios from 'axios';
import store from '../store';
import { getProjectsSuccess, getProjectsFailure, getUsersSuccess, getUsersFailure, getRolesSuccess, getRolesFailure, saveSuccess} from '../actions/actions';



// Get all projects
export function getProjects() {
  return axios.get('/data/data.json')
    .then(response => {
      store.dispatch(getProjectsSuccess(response.data.projects));
      return response.data.projects;
    })
    .catch(e => {
      store.dispatch(getProjectsFailure('server failure'));
    })
}

// Get all users
export function getUsers() {
  return axios.get('/data/data.json')
    .then(response => {
      store.dispatch(getUsersSuccess(response.data.users));
      return response.data.users;
    })
    .catch(e => {
      store.dispatch(getUsersFailure('server failure'));
    })
}

// Get all roles
export function getRoles() {
  return axios.get('/data/data.json')
    .then(response => {
      store.dispatch(getRolesSuccess(response.data.roles));
      return response.data.roles;
    })
    .catch(e => {
      store.dispatch(getRolesFailure('server failure'));
    })
}

// Save
export function saveUserInfo(data) {
  return axios.get('/')
    .then(response => {
      
      let projects = store.getState().projects.projects;
      let users = store.getState().users.users;
      let roles = store.getState().roles.roles;

      let project = projects.find(x => x.id === data.project).name
      let user = users.find(x => x.id === data.user).name
      let role = roles.find(x => x.id === data.role).name

      let find = serverData.find( item => {
        return ((item.project === data.project)&&(item.user === data.user));
      });

      if(find){
        store.dispatch(saveSuccess(`In the project "${project}" the user ${user} already has a role`));
      }else{
        serverData.push(data)
        store.dispatch(saveSuccess(`For the project "${project}" assigned ${user} as ${role}`));
      }

    })
    .catch(e => {})

}

const serverData = [];