import * as types from '../actions/action-types';

const initialState = { users: [], errorMsg: null };

const usersReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.GET_USERS_SUCCESS:
      return {...state, users: action.users };

    case types.GET_USERS_FAILURE:
      return {...state, users: [], errorMsg: users.error }

  }

  return state;
}

export default usersReducer


