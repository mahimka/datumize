import * as types from '../actions/action-types';

const initialState = { projects: [], errorMsg: null };

const projectsReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.GET_PROJECTS_SUCCESS:
      return {...state, projects: action.projects };

    case types.GET_PROJECTS_FAILURE:
      return {...state, projects: [], errorMsg: projects.error }

  }

  return state;
}

export default projectsReducer
