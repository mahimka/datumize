import * as types from '../actions/action-types';

const initialState = { save: null, errorMsg: null };

const saveReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.SAVE_SUCCESS:
      return {...state, save: action.mess };

  }

  return state;
}

export default saveReducer



