import * as types from '../actions/action-types';

export const initialState = { roles: [], errorMsg: null };

const rolesReducer = function(state = initialState, action) {

  switch(action.type) {

    case types.GET_ROLES_SUCCESS:
      return {...state, roles: action.roles, errorMsg: null };

    case types.GET_ROLES_FAILURE:
      return {...state, roles: [], errorMsg: action.error }

  }

  return state;

}

export default rolesReducer