import { combineReducers } from 'redux';

// Reducers
import projects from './projects-reducer';
import users from './users-reducer';
import roles from './roles-reducer';
import save from './save-reducer';

// Combine Reducers
let reducers = combineReducers({
	projects: projects,
	users: users,
	roles: roles,
	save: save,
});

export default reducers;