import * as types from '../actions/action-types';

export function getProjectsSuccess(projects) {
  return { type: types.GET_PROJECTS_SUCCESS, projects };
}
export function getProjectsFailure(error) {
  return { type: types.GET_PROJECTS_FAILURE, error };
}

export function getUsersSuccess(users) {
  return { type: types.GET_USERS_SUCCESS, users };
}
export function getUsersFailure(error) {
  return { type: types.GET_USERS_FAILURE, error };
}

export function getRolesSuccess(roles) {
  return { type: types.GET_ROLES_SUCCESS, roles };
}
export function getRolesFailure(error) {
  return { type: types.GET_ROLES_FAILURE, error };
}


export function saveSuccess(mess) {
  return { type: types.SAVE_SUCCESS, mess };
}