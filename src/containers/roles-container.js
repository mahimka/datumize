import React from 'react';
import { connect } from 'react-redux';
import List from '../components/droplist';
import * as api from '../api/';
import store from '../store';

class RolesContainer extends React.Component {

  componentDidMount() {
    api.getRoles();
  }

  render() {
    return (
      <List values={this.props.roles} handleChange={this.props.handleChange} label={`roles`} value={this.props.value} />
    );
  }

}

const mapStateToProps = function(store) {
  return {
    roles: store.roles.roles
  };
};

export default connect(mapStateToProps)(RolesContainer);
