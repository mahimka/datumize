import React from 'react';
import { connect } from 'react-redux';
import List from '../components/droplist';
import * as api from '../api/';
import store from '../store';

class UsersContainer extends React.Component {

  componentDidMount() {
    api.getUsers();
  }

  render() {
    return (
      <List values={this.props.users} handleChange={this.props.handleChange} label={`users`} value={this.props.value} />
    );
  }

}

const mapStateToProps = function(store) {
  return {
    users: store.users.users
  };
};

export default connect(mapStateToProps)(UsersContainer);
