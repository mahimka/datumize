import React from 'react';
import { connect } from 'react-redux';
import List from '../components/droplist';
import * as api from '../api/';
import store from '../store';

class ProjectsContainer extends React.Component {

  componentDidMount() {
    api.getProjects();
  }

  render() {
    return (
      <List values={this.props.projects} handleChange={this.props.handleChange} label={`projects`} value={this.props.value} />
    );
  }

}

const mapStateToProps = function(store) {
  return {
    projects: store.projects.projects
  };
};

export default connect(mapStateToProps)(ProjectsContainer);
