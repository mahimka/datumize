
import rolesReducer, { initialState } from '../../reducers/roles-reducer'
import * as types from '../../actions/action-types'

describe('roles reducer test', () => {
  
  it('GET_ROLES_SUCCESS', () => {
    const action = {
      type: types.GET_ROLES_SUCCESS,
      roles: [1, 2, 3],
    }

    expect(rolesReducer(initialState, action)).toEqual({
      ...initialState,
      roles: action.roles
    })
  })


  it('GET_ROLES_FAILURE', () => {
    const action = {
      type: types.GET_ROLES_FAILURE,
      roles: [],
      errorMsg: "error"
    }

    expect(rolesReducer(initialState, action)).toEqual({
      ...initialState,
      roles: action.roles,
      errorMsg: action.error
    })
  })



})