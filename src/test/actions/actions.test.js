
import { getRolesSuccess, getRolesFailure } from '../../actions/actions';
import * as types from '../../actions/action-types'

describe('actions test', () => {

  it('getRolesSuccess(): should attach new roles', () => {
    
    const expectedAction = {
      type: types.GET_ROLES_SUCCESS,
      roles: [1,2,3]
    }

    expect(getRolesSuccess([1,2,3])).toEqual(expectedAction)

  })


  it('getRolesFailure(): should return []', () => {
    
    const expectedAction = {
      type: types.GET_ROLES_FAILURE,
      error: 'error'
    }

    expect(getRolesFailure('error')).toEqual(expectedAction)

  })


})