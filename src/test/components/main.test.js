import React from 'react'
import { Provider } from 'react-redux'
import Main from '../../components/main'

import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import renderer from 'react-test-renderer'



const mockStore = configureMockStore([thunk])
const store = mockStore(
  {
    projects: {
      projects: [{id: 1, name: 'Trip to space'}]
    },
    users: {
      users: [{id: 1, name: 'John Doe'}]
    },
    roles: {
      roles: [{id: 1, name: 'Admin'}]
    },
  },
)

describe('Main', () => {
  it('main form renders correctly', () => {
    const component = renderer.create(
      <Provider store={store}>
        <Main />
      </Provider>,
    )
    expect(component.toJSON()).toMatchSnapshot()
  })
})
