import React from 'react';
import { Link } from 'react-router';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

export default class DropList extends React.Component {

  render(){

    return (
      <div className="drop-list">
        <FormControl style={{width: `100%`}}>
          <InputLabel>{(!this.props.value)? this.props.label : ""}</InputLabel>
          <Select
            style={{width: `100%`, maxWidth: `260px`}}
            value={(this.props.value)?this.props.value:""}
            onChange={this.props.handleChange}
          >
            <MenuItem value=""><em>None</em></MenuItem>
              
              {this.props.values.map(val => {
                return (
                  <MenuItem key={val.id} value={val.id}>{val.name}</MenuItem>
                );
              })}

          </Select>
        </FormControl>
      </div>
    );
  }

}