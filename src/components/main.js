import React from 'react';
import { connect } from 'react-redux';

import Users from '../containers/users-container'
import Projects from '../containers/projects-container'
import Roles from '../containers/roles-container'

import * as api from '../api/';

import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import store from '../store';

class Main extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      project: null,
      user: null,
      role: null,
      message: "",
      open: false
    }

    this.handleClick = this.handleClick.bind(this);
    this.projectsChange = this.projectsChange.bind(this);
    this.usersChange = this.usersChange.bind(this);
    this.rolesChange = this.rolesChange.bind(this);
    this.handleCloseSnack = this.handleCloseSnack.bind(this);
  }

  projectsChange(e){
    this.setState({
      project: e.target.value
    })
  }

  usersChange(e){
    this.setState({
      user: e.target.value
    })
  }

  rolesChange(e){
    this.setState({
      role: e.target.value
    })
  }

  componentWillReceiveProps(nextProps) {
    
    if(nextProps.mess.save){
      this.setState({
        message: nextProps.mess.save,
        open: true,
      })

    }
  }


  handleClick() {
    if(this.state.project&&this.state.user&&this.state.role){
      api.saveUserInfo({"project": this.state.project, "user": this.state.user, "role": this.state.role});
    }else{
      this.setState({
        message: `choose all fields`,
        open: true,
      })
    }
  }

  handleCloseSnack(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({
      open: false,
    })
  }


  render() { 
    
    return (
      <div className="main">

          <Projects handleChange={this.projectsChange} value={this.state.project}/>

          <Users handleChange={this.usersChange} value={this.state.user}/>

          <Roles handleChange={this.rolesChange} value={this.state.role}/>

          <Button id="savebtn" variant="contained" color="primary" className="btn" onClick={this.handleClick} style={{width: `100%`, maxWidth: `260px`}}>save</Button>

          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            open={this.state.open}
            autoHideDuration={4000}
            onClose={this.handleCloseSnack}
            message={<span id="message-id">{this.state.message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.handleCloseSnack}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />

      </div>
    );
  }
  
}

const mapStateToProps = function(store) {
  return {
    mess: store.save
  };
};

export default connect(mapStateToProps)(Main);


