# Datumize-test

Aim of the exercise is to generate a form to handle user roles in multiple projects. Each user can
be assigned just one role per project. The list of users, roles, projects and the save action should
execute a call to an API, you can mock it.


### Installing

```
yarn

```

## Run

```
npm start
localhost:8080
```

## Build

```
npm run build

```

## Deploy

```
npm run deploy
https://datumize-test.firebaseapp.com/
```

## Release

```
npm run release

```


## Test

```
npm test

```


## Author

**Max Kuteynikov**